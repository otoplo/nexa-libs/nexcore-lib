'use strict';

var nexcore = module.exports;

// module information
nexcore.version = 'v' + require('./package.json').version;
nexcore.versionGuard = function(version) {
  if (version !== undefined) {
    var message = 'More than one instance of nexcore-lib found. ' +
      'Please make sure to require nexcore-lib and check that submodules do' +
      ' not also include their own nexcore-lib dependency.';
    throw new Error(message);
  }
};
nexcore.versionGuard(global._nexcore);
global._nexcore = nexcore.version;

// crypto
nexcore.crypto = {};
nexcore.crypto.BN = require('./lib/crypto/bn');
nexcore.crypto.ECDSA = require('./lib/crypto/ecdsa');
nexcore.crypto.Schnorr = require('./lib/crypto/schnorr');
nexcore.crypto.Hash = require('./lib/crypto/hash');
nexcore.crypto.Random = require('./lib/crypto/random');
nexcore.crypto.Point = require('./lib/crypto/point');
nexcore.crypto.Signature = require('./lib/crypto/signature');

// encoding
nexcore.encoding = {};
nexcore.encoding.Base58 = require('./lib/encoding/base58');
nexcore.encoding.Base58Check = require('./lib/encoding/base58check');
nexcore.encoding.BufferReader = require('./lib/encoding/bufferreader');
nexcore.encoding.BufferWriter = require('./lib/encoding/bufferwriter');
nexcore.encoding.Varint = require('./lib/encoding/varint');

// utilities
nexcore.util = {};
nexcore.util.buffer = require('./lib/util/buffer');
nexcore.util.js = require('./lib/util/js');
nexcore.util.preconditions = require('./lib/util/preconditions');
nexcore.util.base32 = require('./lib/util/base32');
nexcore.util.convertBits = require('./lib/util/convertBits');

// errors thrown by the library
nexcore.errors = require('./lib/errors');

// main bitcoin library
nexcore.Address = require('./lib/address');
nexcore.GroupToken = require('./lib/grouptoken');
nexcore.Block = require('./lib/block');
nexcore.MerkleBlock = require('./lib/block/merkleblock');
nexcore.BlockHeader = require('./lib/block/blockheader');
nexcore.HDPrivateKey = require('./lib/hdprivatekey.js');
nexcore.HDPublicKey = require('./lib/hdpublickey.js');
nexcore.Message = require('./lib/message');
nexcore.Networks = require('./lib/networks');
nexcore.Opcode = require('./lib/opcode');
nexcore.PrivateKey = require('./lib/privatekey');
nexcore.PublicKey = require('./lib/publickey');
nexcore.Script = require('./lib/script/script');
nexcore.ScriptInterpreter = require('./lib/script/interpreter');
nexcore.Transaction = require('./lib/transaction');
nexcore.URI = require('./lib/uri');
nexcore.Unit = require('./lib/unit');

// dependencies, subject to change
nexcore.deps = {};
nexcore.deps.bnjs = require('bn.js');
nexcore.deps.bs58 = require('bs58');
nexcore.deps.Buffer = Buffer;
nexcore.deps.elliptic = require('elliptic');
nexcore.deps._ = require('lodash');

// Internal usage, exposed for testing/advanced tweaking
nexcore.Transaction.sighash = require('./lib/transaction/sighash');
