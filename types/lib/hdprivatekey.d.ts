/// <reference types="node" />
export = HDPrivateKey;
/**
 * Represents an instance of an hierarchically derived private key.
 *
 * More info on https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki
 *
 * @constructor
 * @param {string|Buffer|Object} arg
 */
declare function HDPrivateKey(arg: string | Buffer | any): HDPrivateKey;
declare class HDPrivateKey {
    /**
     * Represents an instance of an hierarchically derived private key.
     *
     * More info on https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki
     *
     * @constructor
     * @param {string|Buffer|Object} arg
     */
    constructor(arg: string | Buffer | any);
    /**
     * WARNING: This method is deprecated. Use deriveChild or deriveNonCompliantChild instead. This is not BIP32 compliant
     *
     *
     * Get a derived child based on a string or number.
     *
     * If the first argument is a string, it's parsed as the full path of
     * derivation. Valid values for this argument include "m" (which returns the
     * same private key), "m/0/1/40/2'/1000", where the ' quote means a hardened
     * derivation.
     *
     * If the first argument is a number, the child with that index will be
     * derived. If the second argument is truthy, the hardened version will be
     * derived. See the example usage for clarification.
     *
     * @example
     * ```javascript
     * var parent = new HDPrivateKey('xprv...');
     * var child_0_1_2h = parent.derive(0).derive(1).derive(2, true);
     * var copy_of_child_0_1_2h = parent.derive("m/0/1/2'");
     * assert(child_0_1_2h.xprivkey === copy_of_child_0_1_2h);
     * ```
     *
     * @param {string|number} arg
     * @param {boolean?} hardened
     */
    derive(arg: string | number, hardened: boolean | null): any;
    /**
     * WARNING: This method will not be officially supported until v1.0.0.
     *
     *
     * Get a derived child based on a string or number.
     *
     * If the first argument is a string, it's parsed as the full path of
     * derivation. Valid values for this argument include "m" (which returns the
     * same private key), "m/0/1/40/2'/1000", where the ' quote means a hardened
     * derivation.
     *
     * If the first argument is a number, the child with that index will be
     * derived. If the second argument is truthy, the hardened version will be
     * derived. See the example usage for clarification.
     *
     * WARNING: The `nonCompliant` option should NOT be used, except for older implementation
     * that used a derivation strategy that used a non-zero padded private key.
     *
     * @example
     * ```javascript
     * var parent = new HDPrivateKey('xprv...');
     * var child_0_1_2h = parent.deriveChild(0).deriveChild(1).deriveChild(2, true);
     * var copy_of_child_0_1_2h = parent.deriveChild("m/0/1/2'");
     * assert(child_0_1_2h.xprivkey === copy_of_child_0_1_2h);
     * ```
     *
     * @param {string|number} arg
     * @param {boolean?} hardened
     * @return {HDPrivateKey} this, for chaining
     */
    deriveChild(arg: string | number, hardened: boolean | null): HDPrivateKey;
    /**
     * WARNING: This method will not be officially supported until v1.0.0
     *
     *
     * WARNING: If this is a new implementation you should NOT use this method, you should be using
     * `derive` instead.
     *
     * This method is explicitly for use and compatibility with an implementation that
     * was not compliant with BIP32 regarding the derivation algorithm. The private key
     * must be 32 bytes hashing, and this implementation will use the non-zero padded
     * serialization of a private key, such that it's still possible to derive the privateKey
     * to recover those funds.
     *
     * @param {string|number} arg
     * @param {boolean?} hardened
     */
    deriveNonCompliantChild(arg: string | number, hardened: boolean | null): any;
    _deriveWithNumber(index: any, hardened: any, nonCompliant: any): any;
    _deriveFromString(path: any, nonCompliant: any): any;
    _buildFromJSON(arg: any): HDPrivateKey;
    _buildFromObject(arg: any): HDPrivateKey;
    _buildFromSerialized(arg: any): HDPrivateKey;
    _generateRandomly(network: any): HDPrivateKey;
    _calcHDPublicKey(): void;
    _hdPublicKey: import("./hdpublickey");
    /**
     * Receives a object with buffers in all the properties and populates the
     * internal structure
     *
     * @param {Object} arg
     * @param {buffer.Buffer} arg.version
     * @param {buffer.Buffer} arg.depth
     * @param {buffer.Buffer} arg.parentFingerPrint
     * @param {buffer.Buffer} arg.childIndex
     * @param {buffer.Buffer} arg.chainCode
     * @param {buffer.Buffer} arg.privateKey
     * @param {buffer.Buffer} arg.checksum
     * @param {string=} arg.xprivkey - if set, don't recalculate the base58
     *      representation
     * @return {HDPrivateKey} this
     */
    _buildFromBuffers(arg: {
        version: buffer.Buffer;
        depth: buffer.Buffer;
        parentFingerPrint: buffer.Buffer;
        childIndex: buffer.Buffer;
        chainCode: buffer.Buffer;
        privateKey: buffer.Buffer;
        checksum: buffer.Buffer;
        xprivkey?: string | undefined;
    }): HDPrivateKey;
    /**
     * Returns the string representation of this private key (a string starting
     * with "xprv..."
     *
     * @return {string}
     */
    toString(): string;
    /**
     * Returns the console representation of this extended private key.
     * @return {string}
     */
    inspect(): string;
    /**
     * Will return the corresponding hd public key
     *
     * @returns {HDPublicKey} An extended public key generated from the hd private key
     */
    getHDPublicKey(): HDPublicKey;
    /**
     * Will return the corresponding public key
     *
     * @returns {PublicKey} A public key generated from the private key
     */
    getPublicKey(): PublicKey;
    /**
     * Will return the associated private key
     *
     * @returns {PrivateKey} A private key associated to this ext privkey
     */
    getPrivateKey(): PrivateKey;
    /**
     * Returns a plain object with a representation of this private key.
     *
     * Fields include:<ul>
     * <li> network: either 'livenet' or 'testnet'
     * <li> depth: a number ranging from 0 to 255
     * <li> fingerPrint: a number ranging from 0 to 2^32-1, taken from the hash of the
     * <li>     associated public key
     * <li> parentFingerPrint: a number ranging from 0 to 2^32-1, taken from the hash
     * <li>     of this parent's associated public key or zero.
     * <li> childIndex: the index from which this child was derived (or zero)
     * <li> chainCode: an hexa string representing a number used in the derivation
     * <li> privateKey: the private key associated, in hexa representation
     * <li> xprivkey: the representation of this extended private key in checksum
     * <li>     base58 format
     * <li> checksum: the base58 checksum of xprivkey
     * </ul>
     *  @return {Object}
     */
    toObject: () => any;
    toJSON(): any;
    /**
     * Returns a buffer representation of the HDPrivateKey
     *
     * @return {string}
     */
    toBuffer(): string;
}
declare namespace HDPrivateKey {
    /**
     * Verifies that a given path is valid.
     *
     * @param {string|number} arg
     * @param {boolean?} hardened
     * @return {boolean}
     */
    export function isValidPath(arg: string | number, hardened: boolean): boolean;
    /**
     * Internal function that splits a string path into a derivation index array.
     * It will return null if the string path is malformed.
     * It does not validate if indexes are in bounds.
     *
     * @param {string} path
     * @return {Array}
     */
    export function _getDerivationIndexes(path: string): any[];
    /**
     * Verifies that a given serialized private key in base58 with checksum format
     * is valid.
     *
     * @param {string|Buffer} data - the serialized private key
     * @param {string|Network=} network - optional, if present, checks that the
     *     network provided matches the network serialized.
     * @return {boolean}
     */
    export function isValidSerialized(data: string | buffer.Buffer, network?: string | {
        add: (data: {
            name: string;
            alias: string;
            pubkeyhash: number;
            privatekey: number;
            scripthash: number;
            xpubkey: number;
            xprivkey: number;
            networkMagic: number;
            port: number;
            dnsSeeds: any[];
        }) => Network.Network;
        remove: (network: Network.Network) => void;
        defaultNetwork: any;
        livenet: any;
        mainnet: any;
        testnet: any;
        regtest: any;
        get: (arg: string | number | Network.Network, keys: string | any[]) => any;
        enableRegtest: () => void;
        disableRegtest: () => void;
        Network: typeof Network.Network;
    }): boolean;
    /**
     * Checks what's the error that causes the validation of a serialized private key
     * in base58 with checksum to fail.
     *
     * @param {string|Buffer} data - the serialized private key
     * @param {string|Network=} network - optional, if present, checks that the
     *     network provided matches the network serialized.
     * @return {errors.InvalidArgument|null}
     */
    export function getSerializedError(data: string | buffer.Buffer, network?: string | {
        add: (data: {
            name: string;
            alias: string;
            pubkeyhash: number;
            privatekey: number;
            scripthash: number;
            xpubkey: number;
            xprivkey: number;
            networkMagic: number;
            port: number;
            dnsSeeds: any[];
        }) => Network.Network;
        remove: (network: Network.Network) => void;
        defaultNetwork: any;
        livenet: any;
        mainnet: any;
        testnet: any;
        regtest: any;
        get: (arg: string | number | Network.Network, keys: string | any[]) => any;
        enableRegtest: () => void;
        disableRegtest: () => void;
        Network: typeof Network.Network;
    }): any;
    export function _validateNetwork(data: any, networkArg: any): any;
    export function fromString(arg: any): HDPrivateKey;
    export function fromObject(arg: any): HDPrivateKey;
    /**
     * Generate a private key from a seed, as described in BIP32
     *
     * @param {string|Buffer} hexa
     * @param {Network.Network} network
     * @return HDPrivateKey
     */
    export function fromSeed(hexa: string | buffer.Buffer, network?: Network.Network): HDPrivateKey;
    export function _validateBufferArguments(arg: any): void;
    /**
     * Build a HDPrivateKey from a buffer
     *
     * @param {Buffer} arg
     * @return {HDPrivateKey}
     */
    export function fromBuffer(arg: buffer.Buffer): HDPrivateKey;
    export let DefaultDepth: number;
    export let DefaultFingerprint: number;
    export let DefaultChildIndex: number;
    export let Hardened: number;
    export let MaxIndex: number;
    export let RootElementAlias: string[];
    export let VersionSize: number;
    export let DepthSize: number;
    export let ParentFingerPrintSize: number;
    export let ChildIndexSize: number;
    export let ChainCodeSize: number;
    export let PrivateKeySize: number;
    export let CheckSumSize: number;
    export let DataLength: number;
    export let SerializedByteSize: number;
    export let VersionStart: number;
    export let VersionEnd: number;
    import DepthStart = VersionEnd;
    export { DepthStart };
    export let DepthEnd: number;
    import ParentFingerPrintStart = DepthEnd;
    export { ParentFingerPrintStart };
    export let ParentFingerPrintEnd: number;
    import ChildIndexStart = ParentFingerPrintEnd;
    export { ChildIndexStart };
    export let ChildIndexEnd: number;
    import ChainCodeStart = ChildIndexEnd;
    export { ChainCodeStart };
    export let ChainCodeEnd: number;
    export let PrivateKeyStart: number;
    export let PrivateKeyEnd: number;
    import ChecksumStart = PrivateKeyEnd;
    export { ChecksumStart };
    export let ChecksumEnd: number;
}
import buffer = require("buffer");
import PublicKey = require("./publickey");
import PrivateKey = require("./privatekey");
import Network = require("./networks");
//# sourceMappingURL=hdprivatekey.d.ts.map