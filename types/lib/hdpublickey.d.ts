export = HDPublicKey;
/**
 * The representation of an hierarchically derived public key.
 *
 * See https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki
 *
 * @constructor
 * @param {Object|string|Buffer} arg
 */
declare function HDPublicKey(arg: any | string | Buffer): any;
declare class HDPublicKey {
    /**
     * The representation of an hierarchically derived public key.
     *
     * See https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki
     *
     * @constructor
     * @param {Object|string|Buffer} arg
     */
    constructor(arg: any | string | Buffer);
    /**
     * WARNING: This method is deprecated. Use deriveChild instead.
     *
     *
     * Get a derivated child based on a string or number.
     *
     * If the first argument is a string, it's parsed as the full path of
     * derivation. Valid values for this argument include "m" (which returns the
     * same public key), "m/0/1/40/2/1000".
     *
     * Note that hardened keys can't be derived from a public extended key.
     *
     * If the first argument is a number, the child with that index will be
     * derived. See the example usage for clarification.
     *
     * @example
     * ```javascript
     * var parent = new HDPublicKey('xpub...');
     * var child_0_1_2 = parent.derive(0).derive(1).derive(2);
     * var copy_of_child_0_1_2 = parent.derive("m/0/1/2");
     * assert(child_0_1_2.xprivkey === copy_of_child_0_1_2);
     * ```
     *
     * @param {string|number} arg
     */
    derive(arg: string | number, hardened: any): any;
    /**
     * WARNING: This method will not be officially supported until v1.0.0.
     *
     *
     * Get a derivated child based on a string or number.
     *
     * If the first argument is a string, it's parsed as the full path of
     * derivation. Valid values for this argument include "m" (which returns the
     * same public key), "m/0/1/40/2/1000".
     *
     * Note that hardened keys can't be derived from a public extended key.
     *
     * If the first argument is a number, the child with that index will be
     * derived. See the example usage for clarification.
     *
     * @example
     * ```javascript
     * var parent = new HDPublicKey('xpub...');
     * var child_0_1_2 = parent.deriveChild(0).deriveChild(1).deriveChild(2);
     * var copy_of_child_0_1_2 = parent.deriveChild("m/0/1/2");
     * assert(child_0_1_2.xprivkey === copy_of_child_0_1_2);
     * ```
     *
     * @param {string|number} arg
     */
    deriveChild(arg: string | number, hardened: any): any;
    _deriveWithNumber(index: any, hardened: any): any;
    _deriveFromString(path: any): any;
    _buildFromPrivate(arg: any): HDPublicKey;
    _buildFromObject(arg: any): HDPublicKey;
    _buildFromSerialized(arg: any): HDPublicKey;
    /**
     * Receives a object with buffers in all the properties and populates the
     * internal structure
     *
     * @param {Object} arg
     * @param {buffer.Buffer} arg.version
     * @param {buffer.Buffer} arg.depth
     * @param {buffer.Buffer} arg.parentFingerPrint
     * @param {buffer.Buffer} arg.childIndex
     * @param {buffer.Buffer} arg.chainCode
     * @param {buffer.Buffer} arg.publicKey
     * @param {buffer.Buffer} arg.checksum
     * @param {string=} arg.xpubkey - if set, don't recalculate the base58
     *      representation
     * @return {HDPublicKey} this
     */
    _buildFromBuffers(arg: {
        version: buffer.Buffer;
        depth: buffer.Buffer;
        parentFingerPrint: buffer.Buffer;
        childIndex: buffer.Buffer;
        chainCode: buffer.Buffer;
        publicKey: buffer.Buffer;
        checksum: buffer.Buffer;
        xpubkey?: string | undefined;
    }): HDPublicKey;
    /**
     * Returns the base58 checked representation of the public key
     * @return {string} a string starting with "xpub..." in livenet
     */
    toString(): string;
    /**
     * Returns the console representation of this extended public key.
     * @return {string}
     */
    inspect(): string;
    /**
     * Will return the associated public key
     * @return {PublicKey} A public key associated to this ext pubkey
     */
    getPublicKey(): PublicKey;
    /**
     * Returns a plain JavaScript object with information to reconstruct a key.
     *
     * Fields are: <ul>
     *  <li> network: 'livenet' or 'testnet'
     *  <li> depth: a number from 0 to 255, the depth to the master extended key
     *  <li> fingerPrint: a number of 32 bits taken from the hash of the public key
     *  <li> fingerPrint: a number of 32 bits taken from the hash of this key's
     *  <li>     parent's public key
     *  <li> childIndex: index with which this key was derived
     *  <li> chainCode: string in hexa encoding used for derivation
     *  <li> publicKey: string, hexa encoded, in compressed key format
     *  <li> checksum: BufferUtil.integerFromBuffer(this._buffers.checksum),
     *  <li> xpubkey: the string with the base58 representation of this extended key
     *  <li> checksum: the base58 checksum of xpubkey
     * </ul>
     */
    toObject: () => {
        network: any;
        depth: number;
        fingerPrint: number;
        parentFingerPrint: number;
        childIndex: number;
        chainCode: Buffer;
        publicKey: any;
        checksum: number;
        xpubkey: any;
    };
    toJSON(): {
        network: any;
        depth: number;
        fingerPrint: number;
        parentFingerPrint: number;
        childIndex: number;
        chainCode: Buffer;
        publicKey: any;
        checksum: number;
        xpubkey: any;
    };
    /**
     * Return a buffer representation of the xpubkey
     *
     * @return {Buffer}
     */
    toBuffer(): Buffer;
}
declare namespace HDPublicKey {
    /**
     * Verifies that a given path is valid.
     *
     * @param {string|number} arg
     * @return {boolean}
     */
    export function isValidPath(arg: string | number): boolean;
    /**
     * Verifies that a given serialized public key in base58 with checksum format
     * is valid.
     *
     * @param {string|Buffer} data - the serialized public key
     * @param {string|Network=} network - optional, if present, checks that the
     *     network provided matches the network serialized.
     * @return {boolean}
     */
    export function isValidSerialized(data: string | Buffer, network?: string | {
        add: (data: {
            name: string;
            alias: string;
            pubkeyhash: number;
            privatekey: number;
            scripthash: number;
            xpubkey: number;
            xprivkey: number;
            networkMagic: number;
            port: number;
            dnsSeeds: any[];
        }) => Network.Network;
        remove: (network: Network.Network) => void;
        defaultNetwork: any;
        livenet: any;
        mainnet: any;
        testnet: any;
        regtest: any;
        get: (arg: string | number | Network.Network, keys: string | any[]) => any;
        enableRegtest: () => void;
        disableRegtest: () => void;
        Network: typeof Network.Network;
    }): boolean;
    /**
     * Checks what's the error that causes the validation of a serialized public key
     * in base58 with checksum to fail.
     *
     * @param {string|Buffer} data - the serialized public key
     * @param {string|Network=} network - optional, if present, checks that the
     *     network provided matches the network serialized.
     * @return {errors|null}
     */
    export function getSerializedError(data: string | Buffer, network?: string | {
        add: (data: {
            name: string;
            alias: string;
            pubkeyhash: number;
            privatekey: number;
            scripthash: number;
            xpubkey: number;
            xprivkey: number;
            networkMagic: number;
            port: number;
            dnsSeeds: any[];
        }) => Network.Network;
        remove: (network: Network.Network) => void;
        defaultNetwork: any;
        livenet: any;
        mainnet: any;
        testnet: any;
        regtest: any;
        get: (arg: string | number | Network.Network, keys: string | any[]) => any;
        enableRegtest: () => void;
        disableRegtest: () => void;
        Network: typeof Network.Network;
    }): Error;
    export function _validateNetwork(data: any, networkArg: any): any;
    export function _validateBufferArguments(arg: any): void;
    export function fromString(arg: any): HDPublicKey;
    export function fromObject(arg: any): HDPublicKey;
    /**
     * Create a HDPublicKey from a buffer argument
     *
     * @param {Buffer} arg
     * @return {HDPublicKey}
     */
    export function fromBuffer(arg: Buffer): HDPublicKey;
    export let Hardened: number;
    export let RootElementAlias: string[];
    export let VersionSize: number;
    export let DepthSize: number;
    export let ParentFingerPrintSize: number;
    export let ChildIndexSize: number;
    export let ChainCodeSize: number;
    export let PublicKeySize: number;
    export let CheckSumSize: number;
    export let DataSize: number;
    export let SerializedByteSize: number;
    export let VersionStart: number;
    export let VersionEnd: number;
    import DepthStart = VersionEnd;
    export { DepthStart };
    export let DepthEnd: number;
    import ParentFingerPrintStart = DepthEnd;
    export { ParentFingerPrintStart };
    export let ParentFingerPrintEnd: number;
    import ChildIndexStart = ParentFingerPrintEnd;
    export { ChildIndexStart };
    export let ChildIndexEnd: number;
    import ChainCodeStart = ChildIndexEnd;
    export { ChainCodeStart };
    export let ChainCodeEnd: number;
    import PublicKeyStart = ChainCodeEnd;
    export { PublicKeyStart };
    export let PublicKeyEnd: number;
    import ChecksumStart = PublicKeyEnd;
    export { ChecksumStart };
    export let ChecksumEnd: number;
}
import PublicKey = require("./publickey");
import Network = require("./networks");
//# sourceMappingURL=hdpublickey.d.ts.map