export = Varint;
declare function Varint(buf: any): Varint;
declare class Varint {
    constructor(buf: any);
    buf: Buffer;
    set(obj: any): this;
    fromString(str: any): this;
    toString(): string;
    fromBuffer(buf: any): this;
    fromBufferReader(br: any): this;
    fromBN(bn: any): this;
    fromNumber(num: any): this;
    toBuffer(): Buffer;
    toBN(): any;
    toNumber(): any;
}
//# sourceMappingURL=varint.d.ts.map