/**
 * Decode bech32/bech32m string
 * @param {String} str String to decode
 * @returns {Object} Decoded string info
 */
export function decode(str: string): any;
/**
 * Encode using BECH32 encoding
 * @param {String} prefix bech32 prefix
 * @param {Number} version
 * @param {String|Buffer} data
 * @param {String|Number} encoding (optional, default=bech32) Valid encodings are 'bech32', 'bech32m', 0, and 1.
 * @returns {String} encoded string
 */
export function encode(prefix: string, version: number, data: string | Buffer, encoding: string | number): string;
export namespace encodings {
    let BECH32: number;
    let BECH32M: number;
}
//# sourceMappingURL=bech32.d.ts.map