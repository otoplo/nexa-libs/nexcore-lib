export = BufferWriter;
/**
 * @param {object=} obj
 * @returns {BufferWriter}
 * @constructor
 */
declare function BufferWriter(obj?: object | undefined): BufferWriter;
declare class BufferWriter {
    /**
     * @param {object=} obj
     * @returns {BufferWriter}
     * @constructor
     */
    constructor(obj?: object | undefined);
    /** @type {Buffer[]} */
    bufs: Buffer[];
    bufLen: number;
    set(obj: any): this;
    toBuffer(): Buffer;
    concat(): Buffer;
    /**
     * @param {Buffer} buf
     * @returns {BufferWriter}
     */
    write(buf: Buffer): BufferWriter;
    /**
     * @param {Buffer} buf
     * @returns {BufferWriter}
     */
    writeReverse(buf: Buffer): BufferWriter;
    /**
     * @param {number} n
     * @returns {BufferWriter}
     */
    writeUInt8(n: number): BufferWriter;
    /**
     * @param {number} n
     * @returns {BufferWriter}
     */
    writeUInt16BE(n: number): BufferWriter;
    /**
     * @param {number} n
     * @returns {BufferWriter}
     */
    writeUInt16LE(n: number): BufferWriter;
    /**
     * @param {number} n
     * @returns {BufferWriter}
     */
    writeUInt32BE(n: number): BufferWriter;
    /**
     * @param {number} n
     * @returns {BufferWriter}
     */
    writeInt32LE(n: number): BufferWriter;
    /**
     * @param {number} n
     * @returns {BufferWriter}
     */
    writeUInt32LE(n: number): BufferWriter;
    /**
     * @param {BN} bn
     * @returns {BufferWriter}
     */
    writeUInt64BEBN(bn: BN): BufferWriter;
    /**
     * @param {BN} bn
     * @returns {BufferWriter}
     */
    writeUInt64LEBN(bn: BN): BufferWriter;
    /**
     * @param {number} n
     * @returns {BufferWriter}
     */
    writeVarintNum(n: number): BufferWriter;
    /**
     * @param {BN} bn
     * @returns {BufferWriter}
     */
    writeVarintBN(bn: BN): BufferWriter;
    /**
     * @param {Buffer} buf
     * @returns {BufferWriter}
     */
    writeVarLengthBuf(buf: Buffer): BufferWriter;
    /**
     * @param {number} n
     * @returns {BufferWriter}
     */
    writeCoreVarintNum(n: number): BufferWriter;
}
declare namespace BufferWriter {
    /**
     * @param {number} n
     * @returns {Buffer}
     */
    function varintBufNum(n: number): Buffer;
    /**
     * @param {BN} bn
     * @returns {Buffer}
     */
    function varintBufBN(bn: BN): Buffer;
    /**
     * @param {BN} bn
     * @returns {Buffer}
     */
    function groupAmountBufBN(bn: BN): Buffer;
}
import BN = require("bn.js");
//# sourceMappingURL=bufferwriter.d.ts.map