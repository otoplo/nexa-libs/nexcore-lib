export = BufferReader;
/**
 * @param {Buffer|string|object=} buf
 * @returns {BufferReader}
 * @constructor
 */
declare function BufferReader(buf?: (Buffer | string | object) | undefined): BufferReader;
declare class BufferReader {
    /**
     * @param {Buffer|string|object=} buf
     * @returns {BufferReader}
     * @constructor
     */
    constructor(buf?: (Buffer | string | object) | undefined);
    /** @type {Buffer} */
    buf: Buffer;
    pos: number;
    /**
     * @param {{ buf: Buffer?, pos: number? }} obj
     * @returns {BufferReader}
     */
    set(obj: {
        buf: Buffer | null;
        pos: number | null;
    }): BufferReader;
    eof(): boolean;
    finished: any;
    /**
     *
     * @param {number} len
     * @returns {Buffer}
     */
    read(len: number): Buffer;
    readAll(): Buffer;
    readUInt8(): number;
    readUInt16BE(): number;
    readUInt16LE(): number;
    readUInt32BE(): number;
    readUInt32LE(): number;
    readInt32LE(): number;
    /**
     * @returns {BN}
     */
    readUInt64BEBN(): BN;
    readUInt64LEBN(): BN;
    readVarintNum(): number;
    /**
     * reads a length prepended buffer
     */
    readVarLengthBuffer(): Buffer;
    readVarintBuf(): Buffer;
    readVarintBN(): BN;
    reverse(): this;
    /**
     * @param {number} len
     * @returns {Buffer}
     */
    readReverse(len: number): Buffer;
    readCoreVarintNum(): number;
}
import BN = require("bn.js");
//# sourceMappingURL=bufferreader.d.ts.map