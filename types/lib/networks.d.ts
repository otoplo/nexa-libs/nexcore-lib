/**
 * @function
 * @member Networks#add
 * Will add a custom Network
 * @param {Object} data
 * @param {string} data.name - The name of the network
 * @param {string} data.alias - The aliased name of the network
 * @param {Number} data.pubkeyhash - The publickey hash prefix
 * @param {Number} data.privatekey - The privatekey prefix
 * @param {Number} data.scripthash - The scripthash prefix
 * @param {Number} data.xpubkey - The extended public key magic
 * @param {Number} data.xprivkey - The extended private key magic
 * @param {Number} data.networkMagic - The network magic number
 * @param {Number} data.port - The network port
 * @param {Array}  data.dnsSeeds - An array of dns seeds
 * @return Network
 */
declare function addNetwork(data: {
    name: string;
    alias: string;
    pubkeyhash: number;
    privatekey: number;
    scripthash: number;
    xpubkey: number;
    xprivkey: number;
    networkMagic: number;
    port: number;
    dnsSeeds: any[];
}): Network;
/**
 * @function
 * @member Networks#remove
 * Will remove a custom network
 * @param {Network} network
 */
declare function removeNetwork(network: Network): void;
export var livenet: any;
export var testnet: any;
export var regtest: any;
/**
 * @function
 * @member Networks#get
 * Retrieves the network associated with a magic number or string.
 * @param {string|number|Network} arg
 * @param {string|Array} keys - if set, only check if the magic number associated with this name matches
 * @return Network
 */
export function get(arg: string | number | Network, keys: string | any[]): any;
/**
 * @function
 * @deprecated
 * @member Networks#enableRegtest
 * Will enable regtest features for testnet
 */
export function enableRegtest(): void;
/**
 * @function
 * @deprecated
 * @member Networks#disableRegtest
 * Will disable regtest features for testnet
 */
export function disableRegtest(): void;
/**
 * A network is merely a map containing values that correspond to version
 * numbers for each nexa network. Currently only supporting "livenet"
 * (a.k.a. "mainnet") and "testnet".
 * @constructor
 */
export function Network(): void;
export class Network {
    toString(): any;
}
export { addNetwork as add, removeNetwork as remove, livenet as defaultNetwork, livenet as mainnet };
//# sourceMappingURL=networks.d.ts.map