export = Message;
declare function Message(message: any): Message;
declare class Message {
    constructor(message: any);
    message: any;
    magicHash(): Buffer;
    _sign(privateKey: any): any;
    /**
     * Will sign a message with a given bitcoin private key.
     *
     * @param {PrivateKey} privateKey - An instance of PrivateKey
     * @returns {String} A base64 encoded compact signature
     */
    sign(privateKey: PrivateKey): string;
    _verify(publicKey: any, signature: any): any;
    error: string;
    /**
     * Will return a boolean of the signature is valid for a given nexa address.
     * If it isn't the specific reason is accessible via the "error" member.
     *
     * @param {Address|String} nexaAddress - A nexa address
     * @param {String} signatureString - A base64 encoded compact signature
     * @returns {Boolean}
     */
    verify(nexaAddress: Address | string, signatureString: string): boolean;
    /**
     * Will return a public key string if the provided signature and the message digest is correct
     * If it isn't the specific reason is accessible via the "error" member.
     *
     * @param {Address|String} nexaAddress - A nexa address
     * @param {String} signatureString - A base64 encoded compact signature
     * @returns {String}
     */
    recoverPublicKey(nexaAddress: Address | string, signatureString: string): string;
    /**
     * @returns {Object} A plain object with the message information
     */
    toObject(): any;
    /**
     * @returns {String} A JSON representation of the message information
     */
    toJSON(): string;
    /**
     * Will return a the string representation of the message
     *
     * @returns {String} Message
     */
    toString(): string;
    /**
     * Will return a string formatted for the console
     *
     * @returns {String} Message
     */
    inspect(): string;
}
declare namespace Message {
    let MAGIC_BYTES: Buffer;
    /**
     * Instantiate a message from a message string
     *
     * @param {String} str - A string of the message
     * @returns {Message} A new instance of a Message
     */
    function fromString(str: string): Message;
    /**
     * Instantiate a message from JSON
     *
     * @param {String} json - An JSON string or Object with keys: message
     * @returns {Message} A new instance of a Message
     */
    function fromJSON(json: string): Message;
}
import PrivateKey = require("./privatekey");
import Address = require("./address");
//# sourceMappingURL=message.d.ts.map