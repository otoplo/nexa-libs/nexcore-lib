declare const _exports: {
    (arg: any): import("./block");
    new (arg: any): import("./block");
    MAX_BLOCK_SIZE: number;
    _from(arg: any): any;
    _fromObject(data: any): any;
    fromObject(obj: any): import("./block");
    _fromBufferReader(br: import("../encoding/bufferreader")): any;
    fromBufferReader(br: import("../encoding/bufferreader")): import("./block");
    fromBuffer(buf: Buffer): import("./block");
    fromString(str: string): import("./block");
    fromRawBlock(data: BinaryData | Buffer): import("./block");
    Values: {
        START_OF_BLOCK: number;
        NULL_HASH: Buffer;
    };
    BlockHeader: typeof import("./blockheader");
    MerkleBlock: typeof import("./merkleblock");
};
export = _exports;
export const BlockHeader: typeof import("./blockheader");
export const MerkleBlock: typeof import("./merkleblock");
//# sourceMappingURL=index.d.ts.map