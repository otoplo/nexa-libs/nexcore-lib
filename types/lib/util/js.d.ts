/**
 * Determines whether a string contains only hexadecimal values
 *
 * @name JSUtil.isHexa
 * @param {string} value
 * @return {boolean} true if the string is the hexa representation of a number
 */
export function isHexa(value: string): boolean;
export declare function isValidJSON(arg: string): any;
export declare function cloneArray(array: any): any[];
export declare function defineImmutable(target: any, values: any): any;
export declare function isNaturalNumber(value: any): boolean;
export { isHexa as isHexaString };
//# sourceMappingURL=js.d.ts.map