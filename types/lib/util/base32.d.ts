/***
 * Encodes the given array of 5-bit integers as a base32-encoded string.
 *
 * @param {Array} data Array of integers between 0 and 31 inclusive.
 */
export function encode(data: any[]): string;
/***
 * Decodes the given base32-encoded string into an array of 5-bit integers.
 *
 * @param {string} base32
 */
export function decode(base32: string): any[];
//# sourceMappingURL=base32.d.ts.map