export function checkState(condition: any, message: any): void;
export function checkArgument(condition: any, argumentName: any, message: any, docsPath: any): void;
export function checkArgumentType(argument: any, type: any, argumentName: any): void;
//# sourceMappingURL=preconditions.d.ts.map