export = Unit;
/**
 * Utility for handling and converting nexa units. The supported units are
 * MEX, KEX, NEXA (also named NEX) and satoshis. A unit instance can be created with an
 * amount and a unit code, or alternatively using static methods like {fromNEXA}.
 * It also allows to be created from a fiat amount and the exchange rate, or
 * alternatively using the {fromFiat} static method.
 * You can consult for different representation of a unit instance using it's
 * {to} method, the fixed unit methods like {toSatoshis} or alternatively using
 * the unit accessors. It also can be converted to a fiat amount by providing the
 * corresponding NEXA/fiat exchange rate.
 *
 * @example
 * ```javascript
 * var sats = Unit.fromNEXA(5.46).toSatoshis();
 * var nex = Unit.fromMEX(5.46).to(Unit.NEX);
 * var kex = Unit.fromFiat(5.46, 350).KEX;
 * var mex = new Unit(5.46, Unit.NEXA).MEX;
 * ```
 *
 * @param {Number} amount - The amount to be represented
 * @param {String|Number} code - The unit of the amount or the exchange rate
 * @returns {Unit} A new instance of an Unit
 * @constructor
 */
declare function Unit(amount: number, code: string | number): Unit;
declare class Unit {
    /**
     * Utility for handling and converting nexa units. The supported units are
     * MEX, KEX, NEXA (also named NEX) and satoshis. A unit instance can be created with an
     * amount and a unit code, or alternatively using static methods like {fromNEXA}.
     * It also allows to be created from a fiat amount and the exchange rate, or
     * alternatively using the {fromFiat} static method.
     * You can consult for different representation of a unit instance using it's
     * {to} method, the fixed unit methods like {toSatoshis} or alternatively using
     * the unit accessors. It also can be converted to a fiat amount by providing the
     * corresponding NEXA/fiat exchange rate.
     *
     * @example
     * ```javascript
     * var sats = Unit.fromNEXA(5.46).toSatoshis();
     * var nex = Unit.fromMEX(5.46).to(Unit.NEX);
     * var kex = Unit.fromFiat(5.46, 350).KEX;
     * var mex = new Unit(5.46, Unit.NEXA).MEX;
     * ```
     *
     * @param {Number} amount - The amount to be represented
     * @param {String|Number} code - The unit of the amount or the exchange rate
     * @returns {Unit} A new instance of an Unit
     * @constructor
     */
    constructor(amount: number, code: string | number);
    _value: number;
    _from(amount: any, code: any): number;
    /**
     * Returns the value represented in the specified unit
     *
     * @param {String|Number} code - The unit code or exchange rate
     * @returns {Number} The converted value
     */
    to(code: string | number): number;
    /**
     * Returns the value represented in NEXA
     *
     * @returns {Number} The value converted to NEXA
     */
    toNEX: () => number;
    toNEXA(): number;
    /**
     * Returns the value represented in KEX
     *
     * @returns {Number} The value converted to KEX
     */
    toKEX(): number;
    /**
     * Returns the value represented in MEX
     *
     * @returns {Number} The value converted to MEX
     */
    toMEX(): number;
    /**
     * Returns the value represented in satoshis
     *
     * @returns {Number} The value converted to satoshis
     */
    toSatoshis(): number;
    /**
     * Returns the value represented in fiat
     *
     * @param {number} rate - The exchange rate between NEXA/currency
     * @returns {Number} The value converted to fiat
     */
    atRate(rate: number): number;
    /**
     * Returns a the string representation of the value in satoshis
     *
     * @returns {string} the value in satoshis
     */
    toString(): string;
    /**
     * Returns a plain object representation of the Unit
     *
     * @returns {Object} An object with the keys: amount and code
     */
    toObject: () => any;
    toJSON(): any;
    /**
     * Returns a string formatted for the console
     *
     * @returns {string} the value in satoshis
     */
    inspect(): string;
}
declare namespace Unit {
    /**
     * Returns a Unit instance created from JSON string or object
     *
     * @param {String|Object} json - JSON with keys: amount and code
     * @returns {Unit} A Unit instance
     */
    function fromObject(data: any): Unit;
    function fromNEX(amount: number): Unit;
    function fromNEXA(amount: number): Unit;
    /**
     * Returns a Unit instance created from an amount in KEX
     *
     * @param {Number} amount - The amount in KEX
     * @returns {Unit} A Unit instance
     */
    function fromKEX(amount: number): Unit;
    /**
     * Returns a Unit instance created from an amount in MEX
     *
     * @param {Number} amount - The amount in MEX
     * @returns {Unit} A Unit instance
     */
    function fromMEX(amount: number): Unit;
    /**
     * Returns a Unit instance created from an amount in satoshis
     *
     * @param {Number} amount - The amount in satoshis
     * @returns {Unit} A Unit instance
     */
    function fromSatoshis(amount: number): Unit;
    /**
     * Returns a Unit instance created from a fiat amount and exchange rate.
     *
     * @param {Number} amount - The amount in fiat
     * @param {Number} rate - The exchange rate NEXA/fiat
     * @returns {Unit} A Unit instance
     */
    function fromFiat(amount: number, rate: number): Unit;
}
//# sourceMappingURL=unit.d.ts.map