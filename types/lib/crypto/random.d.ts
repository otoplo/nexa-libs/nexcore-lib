export = Random;
declare function Random(): void;
declare namespace Random {
    function getRandomBuffer(size: any): Buffer;
    function getRandomBufferNode(size: any): Buffer;
    function getRandomBufferBrowser(size: any): Buffer;
    function getPseudoRandomBuffer(size: any): Buffer;
}
//# sourceMappingURL=random.d.ts.map