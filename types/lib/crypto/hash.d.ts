export function sha1(buf: any): Buffer;
export namespace sha1 {
    let blocksize: number;
}
export function sha256(buf: any): Buffer;
export namespace sha256 {
    let blocksize_1: number;
    export { blocksize_1 as blocksize };
}
export function sha256sha256(buf: any): Buffer;
export function ripemd160(buf: any): Buffer;
export function sha256ripemd160(buf: any): Buffer;
export function sha512(buf: any): Buffer;
export namespace sha512 {
    let blocksize_2: number;
    export { blocksize_2 as blocksize };
}
export function hmac(hashf: any, data: any, key: any): any;
export function sha256hmac(data: any, key: any): any;
export function sha512hmac(data: any, key: any): any;
//# sourceMappingURL=hash.d.ts.map