export = ECDSA;
declare function ECDSA(obj: any): ECDSA;
declare class ECDSA {
    constructor(obj: any);
    set(obj: any): this;
    hashbuf: any;
    endian: any;
    privkey: any;
    pubkey: any;
    sig: any;
    k: any;
    verified: any;
    privkey2pubkey(): void;
    calci(): this;
    randomK(): this;
    deterministicK(badrs: any): this;
    toPublicKey(): PublicKey;
    sigError(): false | "hashbuf must be a 32 byte buffer" | "r and s not in range" | "p is infinity" | "Invalid signature";
    _findSignature(d: any, e: any): {
        s: any;
        r: any;
    };
    sign(): this;
    signRandomK(): this;
    toString(): string;
    verify(): this;
}
declare namespace ECDSA {
    function fromString(str: any): ECDSA;
    function toLowS(s: any): any;
    function sign(hashbuf: any, privkey: any, endian: any): any;
    function verify(hashbuf: any, sig: any, pubkey: any, endian: any): any;
}
import PublicKey = require("../publickey");
//# sourceMappingURL=ecdsa.d.ts.map