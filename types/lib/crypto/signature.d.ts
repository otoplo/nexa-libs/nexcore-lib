export = Signature;
declare function Signature(r: any, s: any, isSchnorr: any): Signature;
declare class Signature {
    constructor(r: any, s: any, isSchnorr: any);
    set(obj: any): this;
    r: any;
    s: any;
    i: any;
    compressed: any;
    isSchnorr: any;
    nhashtype: any;
    toCompact(i: any, compressed: any): Buffer;
    toBuffer: () => Buffer;
    toDER(): Buffer;
    toString(): string;
    /**
     * Compares to bitcoind's IsLowDERSignature
     * See also ECDSA signature algorithm which enforces this.
     * See also BIP 62, "low S values in signatures"
     */
    hasLowS(): boolean;
    /**
     * @returns true if the nhashtype is exactly equal to one of the standard options or combinations thereof.
     * Translated from bitcoind's IsDefinedHashtypeSignature
     */
    hasDefinedHashtype(): boolean;
    toTxFormat(signingMethod: any): Buffer;
}
declare namespace Signature {
    function fromCompact(buf: any): Signature;
    function fromDER(buf: any, strict: any): Signature;
    function fromBuffer(buf: any, strict: any): Signature;
    function fromTxFormat(buf: any): any;
    function fromDataFormat(buf: any): any;
    function fromString(str: any): Signature;
    function fromTxString(str: any, encoding?: string): any;
    function parseSchnorrEncodedSig(buf: any): {
        r: any;
        s: any;
        nhashtype: any;
    };
    /**
     * In order to mimic the non-strict DER encoding of OpenSSL, set strict = false.
     */
    function parseDER(buf: any, strict: any): {
        header: any;
        length: any;
        rheader: any;
        rlength: any;
        rneg: boolean;
        rbuf: any;
        r: any;
        sheader: any;
        slength: any;
        sneg: boolean;
        sbuf: any;
        s: any;
    };
    function isTxDER(buf: any): boolean;
    /**
     * This function is translated from bitcoind's IsDERSignature and is used in
     * the script interpreter.  This "DER" format actually includes an extra byte,
     * the nhashtype, at the end. It is really the tx format, not DER format.
     *
     * A canonical signature exists of: [30] [total len] [02] [len R] [R] [02] [len S] [S]
     * Where R and S are not negative (their first byte has its highest bit not set), and not
     * excessively padded (do not start with a 0 byte, unless an otherwise negative number follows,
     * in which case a single 0 byte is necessary and even required).
     *
     * See https://bitcointalk.org/index.php?topic=8392.msg127623#msg127623
     */
    function isDER(buf: any): boolean;
    let SIGHASH_NEXA_ALL: number;
    let SIGHASH_ALL: number;
    let SIGHASH_NONE: number;
    let SIGHASH_SINGLE: number;
    let SIGHASH_FORKID: number;
    let SIGHASH_ANYONECANPAY: number;
}
//# sourceMappingURL=signature.d.ts.map