export = PublicKeyTemplateInput;
/**
 * Represents a special kind of input of PayToPublicKeyTemplate (well-known PayToScriptTemplate) kind.
 * @constructor
 */
declare function PublicKeyTemplateInput(...args: any[]): void;
declare class PublicKeyTemplateInput {
    /**
     * Represents a special kind of input of PayToPublicKeyTemplate (well-known PayToScriptTemplate) kind.
     * @constructor
     */
    constructor(...args: any[]);
    /**
     * @param {Transaction} transaction - the transaction to be signed
     * @param {PrivateKey} privateKey - the private key with which to sign the transaction
     * @param {number} index - the index of the input in the transaction input vector
     * @param {number=} sigtype - the type of signature, defaults to Signature.SIGHASH_ALL
     * @param {Buffer=} hashData - the precalculated hash of the public key associated with the privateKey provided
     * @param {String} signingMethod - the signing method used to sign tx "ecdsa" or "schnorr"
     * @return {Array} of objects that can be
     */
    getSignatures(transaction: Transaction, privateKey: PrivateKey, index: number, sigtype?: number | undefined, hashData?: Buffer | undefined, signingMethod: string): any[];
    /**
     * Add the provided signature
     *
     * @param {Object} signature
     * @param {PublicKey} signature.publicKey
     * @param {Signature} signature.signature
     * @param {number=} signature.sigtype
     * @param {String} signingMethod only "schnorr" allowed
     * @return {PublicKeyTemplateInput} this, for chaining
     */
    addSignature(transaction: any, signature: {
        publicKey: PublicKey;
        signature: Signature;
        sigtype?: number | undefined;
    }, signingMethod: string): PublicKeyTemplateInput;
    isValidSignature(transaction: any, signature: any, signingMethod: any): boolean;
    /**
     * Clear the input's signature
     * @return {PublicKeyTemplateInput} this, for chaining
     */
    clearSignatures(): PublicKeyTemplateInput;
    /**
     * Query whether the input is signed
     * @return {boolean}
     */
    isFullySigned(): boolean;
    _estimateSize(): number;
}
declare namespace PublicKeyTemplateInput {
    let SCRIPT_MAX_SIZE: number;
}
import Signature = require("../../crypto/signature");
//# sourceMappingURL=publickeytemplate.d.ts.map