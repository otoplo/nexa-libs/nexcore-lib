declare const _exports: {
    (params: any): import("./input");
    new (params: any): import("./input");
    MAXINT: number;
    DEFAULT_SEQNUMBER: number;
    DEFAULT_LOCKTIME_SEQNUMBER: number;
    DEFAULT_RBF_SEQNUMBER: number;
    SEQUENCE_LOCKTIME_TYPE_FLAG: number;
    DEFAULT_TYPE: number;
    fromObject(obj: any): import("./input");
    fromBufferReader(br: any): import("./input");
    PublicKey: typeof import("./publickey");
    PublicKeyHash: typeof import("./publickeyhash");
    PublicKeyTemplate: typeof import("./publickeytemplate");
    MultiSig: typeof import("./multisig.js");
    MultiSigScriptHash: typeof import("./multisigscripthash.js");
    Escrow: typeof import("./escrow.js");
    ScriptTemplate: typeof import("./scripttemplate");
};
export = _exports;
export const PublicKey: typeof import("./publickey");
export const PublicKeyHash: typeof import("./publickeyhash");
export const PublicKeyTemplate: typeof import("./publickeytemplate");
export const MultiSig: typeof import("./multisig.js");
export const MultiSigScriptHash: typeof import("./multisigscripthash.js");
export const Escrow: typeof import("./escrow.js");
export const ScriptTemplate: typeof import("./scripttemplate");
//# sourceMappingURL=index.d.ts.map