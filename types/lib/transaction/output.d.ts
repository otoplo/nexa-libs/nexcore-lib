export = Output;
declare function Output(args: any): Output;
declare class Output {
    constructor(args: any);
    type: any;
    satoshis: any;
    _scriptBuffer: any;
    get script(): import("../script/script");
    invalidSatoshis(): false | "transaction txout satoshis greater than max safe integer" | "transaction txout satoshis has corrupted value" | "transaction txout negative";
    set satoshisBN(arg: any);
    get satoshisBN(): any;
    toObject: () => {
        type: any;
        satoshis: any;
    };
    toJSON(): {
        type: any;
        satoshis: any;
    };
    setScriptFromBuffer(buffer: any): void;
    _script: import("../script/script");
    setScript(script: any): this;
    inspect(): string;
    toBufferWriter(writer: any): any;
}
declare namespace Output {
    function fromObject(data: any): Output;
    function fromBufferReader(br: any): Output;
}
//# sourceMappingURL=output.d.ts.map