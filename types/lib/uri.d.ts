export = URI;
/**
 * Nexcore URI
 *
 * Instantiate an URI from a nexa URI String or an Object. An URI instance
 * can be created with a nexa uri string or an object. All instances of
 * URI are valid, the static method isValid allows checking before instantiation.
 *
 * All standard parameters can be found as members of the class, the address
 * is represented using an {Address} instance and the amount is represented in
 * satoshis. Any other non-standard parameters can be found under the extra member.
 *
 * @example
 * ```javascript
 *
 * var uri = new URI('nexa:nqtsq5g5f97xxcj846gaduz8p7940mldglqzcgm30vfwqxtj?amount=5000000.46');
 * console.log(uri.address, uri.amount);
 * ```
 *
 * @param {string|Object} data - A nexa URI string or an Object
 * @param {Array.<string>=} knownParams - Required non-standard params
 * @throws {TypeError} Invalid nexa address
 * @throws {TypeError} Invalid amount
 * @throws {Error} Unknown required argument
 * @returns {URI} A new valid and frozen instance of URI
 * @constructor
 */
declare function URI(data: string | any, knownParams?: Array<string> | undefined): URI;
declare class URI {
    /**
     * Nexcore URI
     *
     * Instantiate an URI from a nexa URI String or an Object. An URI instance
     * can be created with a nexa uri string or an object. All instances of
     * URI are valid, the static method isValid allows checking before instantiation.
     *
     * All standard parameters can be found as members of the class, the address
     * is represented using an {Address} instance and the amount is represented in
     * satoshis. Any other non-standard parameters can be found under the extra member.
     *
     * @example
     * ```javascript
     *
     * var uri = new URI('nexa:nqtsq5g5f97xxcj846gaduz8p7940mldglqzcgm30vfwqxtj?amount=5000000.46');
     * console.log(uri.address, uri.amount);
     * ```
     *
     * @param {string|Object} data - A nexa URI string or an Object
     * @param {Array.<string>=} knownParams - Required non-standard params
     * @throws {TypeError} Invalid nexa address
     * @throws {TypeError} Invalid amount
     * @throws {Error} Unknown required argument
     * @returns {URI} A new valid and frozen instance of URI
     * @constructor
     */
    constructor(data: string | any, knownParams?: Array<string> | undefined);
    extras: {};
    knownParams: string[];
    address: Address;
    network: any;
    amount: any;
    message: any;
    /**
     * Internal function to load the URI instance with an object.
     *
     * @param {Object} obj - Object with the information
     * @throws {TypeError} Invalid nexa address
     * @throws {TypeError} Invalid amount
     * @throws {Error} Unknown required argument
     */
    _fromObject(obj: any): void;
    /**
     * Internal function to transform a NEXA string amount into satoshis
     *
     * @param {string} amount - Amount NEXA string
     * @throws {TypeError} Invalid amount
     * @returns {number} Amount represented in satoshis
     */
    _parseAmount(amount: string): number;
    toObject: () => {};
    toJSON(): {};
    /**
     * Will return a the string representation of the URI
     *
     * @returns {string} Nexa URI string
     */
    toString(): string;
    /**
     * Will return a string formatted for the console
     *
     * @returns {string} Bitcoin URI
     */
    inspect(): string;
}
declare namespace URI {
    /**
     * Instantiate a URI from a String
     *
     * @param {string} str - JSON string or object of the URI
     * @returns {URI} A new instance of a URI
     */
    function fromString(str: string): URI;
    /**
     * Instantiate a URI from an Object
     *
     * @param {Object} data - object of the URI
     * @returns {URI} A new instance of a URI
     */
    function fromObject(json: any): URI;
    /**
     * Check if an bitcoin URI string is valid
     *
     * @example
     * ```javascript
     *
     * var valid = URI.isValid('nexa:nqtsq5g5f97xxcj846gaduz8p7940mldglqzcgm30vfwqxtj');
     * // true
     * ```
     *
     * @param {string|Object} data - A nexa URI string or an Object
     * @param {Array.<string>=} knownParams - Required non-standard params
     * @returns {boolean} Result of uri validation
     */
    function isValid(arg: any, knownParams?: string[]): boolean;
    /**
     * Convert a nexa URI string into a simple object.
     *
     * @param {string} uri - A nexa URI string
     * @throws {TypeError} Invalid nexa URI
     * @returns {Object} An object with the parsed params
     */
    function parse(uri: string): any;
    let Members: string[];
}
import Address = require("./address");
//# sourceMappingURL=uri.d.ts.map