'use strict';

var inherits = require('inherits');

var $ = require('../../util/preconditions');
var BufferUtil = require('../../util/buffer');

var Hash = require('../../crypto/hash');
var Input = require('./input');
var Output = require('../output');
var Sighash = require('../sighash');
var Script = require('../../script');
var Signature = require('../../crypto/signature');
var TransactionSignature = require('../signature');
var Opcode = require('../../opcode');

/**
 * Represents a special kind of input of PayToPublicKeyTemplate (well-known PayToScriptTemplate) kind.
 * @constructor
 */
function PublicKeyTemplateInput() {
  Input.apply(this, arguments);
}
inherits(PublicKeyTemplateInput, Input);

/* jshint maxparams: 5 */
/**
 * @param {Transaction} transaction - the transaction to be signed
 * @param {PrivateKey} privateKey - the private key with which to sign the transaction
 * @param {number} index - the index of the input in the transaction input vector
 * @param {number=} sigtype - the type of signature, defaults to Signature.SIGHASH_ALL
 * @param {Buffer=} hashData - the precalculated hash of the public key associated with the privateKey provided
 * @param {String} signingMethod - the signing method used to sign tx "ecdsa" or "schnorr"
 * @return {Array} of objects that can be
 */
PublicKeyTemplateInput.prototype.getSignatures = function(transaction, privateKey, index, sigtype, hashData, signingMethod) {
  $.checkState(this.output instanceof Output);
  hashData = hashData || Hash.sha256ripemd160(Script.empty().add(privateKey.publicKey.toBuffer()).toBuffer());
  
  if (BufferUtil.equals(hashData, this.output.script.getPublicKeyTemplate())) {
    var subscript = Script.empty().add(Opcode.OP_FROMALTSTACK).add(Opcode.OP_CHECKSIGVERIFY);
    return [new TransactionSignature({
      publicKey: privateKey.publicKey,
      prevTxId: this.prevTxId,
      outputIndex: this.outputIndex,
      inputIndex: index,
      signature: Sighash.sign(transaction, privateKey, sigtype, index, subscript, this.output.satoshisBN, undefined, signingMethod),
      sigtype: sigtype
    })];
  }
  return [];
};
/* jshint maxparams: 3 */

/**
 * Add the provided signature
 *
 * @param {Object} signature
 * @param {PublicKey} signature.publicKey
 * @param {Signature} signature.signature
 * @param {number=} signature.sigtype
 * @param {String} signingMethod only "schnorr" allowed
 * @return {PublicKeyTemplateInput} this, for chaining
 */
PublicKeyTemplateInput.prototype.addSignature = function(transaction, signature, signingMethod) {

  $.checkState(this.isValidSignature(transaction, signature, signingMethod), 'Signature is invalid');

  this.setScript(Script.buildPublicKeyTemplateIn(
    signature.publicKey,
    signature.signature.toDER(),
    signature.sigtype
  ));
  return this;
};

PublicKeyTemplateInput.prototype.isValidSignature = function(transaction, signature, signingMethod) {
  var subscript = Script.empty().add(Opcode.OP_FROMALTSTACK).add(Opcode.OP_CHECKSIGVERIFY);
  signature.signature.nhashtype = signature.sigtype;
  return Sighash.verify(
    transaction,
    signature.signature,
    signature.publicKey,
    signature.inputIndex,
    subscript,
    this.output.satoshisBN,
    undefined,
    signingMethod
  );
};

/**
 * Clear the input's signature
 * @return {PublicKeyTemplateInput} this, for chaining
 */
PublicKeyTemplateInput.prototype.clearSignatures = function() {
  this.setScript(Script.empty());
  return this;
};

/**
 * Query whether the input is signed
 * @return {boolean}
 */
PublicKeyTemplateInput.prototype.isFullySigned = function() {
  return this.script.isPublicKeyTemplateIn();
};

PublicKeyTemplateInput.SCRIPT_MAX_SIZE = 100; // sigsize (1 + 64) + pubkey push script (1 + 34)

PublicKeyTemplateInput.prototype._estimateSize = function() {
  // type + outpoint + scriptlen + script + sequence + amount
  return 1 + 32 + 1 + PublicKeyTemplateInput.SCRIPT_MAX_SIZE + 4 + 8;
};

module.exports = PublicKeyTemplateInput;
